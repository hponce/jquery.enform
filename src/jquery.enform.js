

(function($, undefined) {

    $.fn.enform = function(options) {

        var defaults = {
            clientId: 0,
            campaignId: 0,
		    pageId: 0,
            mandatory: {},
            validation: {},
            success: function() {
                alert("Inscription réussie !");
            },
            error: function(msg) {
				if (typeof msg == "string") {
					msg = [msg];
				}
				
                alert(msg.join("\n"));
            },
            init: function() {},
            complete: function() {},
			data: function(d) {return d;},
            default_error: "Une erreur est survenue lors de votre inscription.",
            default_text_separator: "<br/><br/>"
        };
        var settings;
        var selector = this;
        var placeholder = ('placeholder' in document.createElement('input'));
        

        function extractQueryString() {
	        var args = window.location.search.substr(1).split('&');
            var params = {};

	        for (var i = 0, l = args.length; i < l; i++) {
		        var val = args[i].split('=');

		        if (val.length === 2) {
			        params[ val[0] ] = decodeURIComponent(val[1]);
		        }
	        }

	        return params;
        }

        

        function prepareSupporterData(data) {
            var query_string = extractQueryString();
            var form_data = {};
            
            if (settings.pageId === 0) {
			    for (var i in query_string) {
                    var value = query_string[i];
			        
				    switch (i) {
					    case 'utm_campaign':
					    case 'utm_medium':
					    case 'utm_source':
					    case 'utm_term':
					    case 'utm_content':
					    case 'ea.tracking.id':
                            form_data[i] = value;
						    break;
				    }
			    }

			    form_data["ea_requested_action"] = "ea_submit_user_form";
                form_data["ea_javascript_enabled"] = "true";
                form_data["ea.AJAX.submit"] = "true";
                form_data["ea.submitted.page"] = "1";
                form_data["format"] = "json";

                if (settings.clientId && settings.clientId > 0) {
                    form_data["ea.client.id"] = settings.clientId;
                }

                if (settings.campaignId && settings.campaignId > 0) {
                    form_data["ea.campaign.id"] = settings.campaignId;
                }
                
                for (name in data) {
                    var value = data[name];
                    var key = name;
                    if ('@' === name.substr(0, 1)) {
                        key = name.substr(1);
                    }
                    
                    form_data[key] = value;
                }

            }
            else {
                
                form_data = {
                    supporter: {
                        questions: {}
                    }
                };

                for (var i in query_string) {
                    var value = query_string[i];
                    switch(i) {
                        case 'utm_source':
                            form_data.txn1 = value;
                            break;
                        case 'utm_medium':
                            form_data.txn2 = value;
                            break;
                        case 'utm_campaign':
                            form_data.txn3 = value;
                            break;
                        case 'utm_content':
                            form_data.txn4 = value;
                            break;
                        case 'utm_term':
                            form_data.txn5 = value;
                            break;
                        case 'ea.tracking.id':
                            form_data.trackingId = value;
                            break;
                    }
                }
                
                for (name in data) {
                    var value = data[name];
                    var pos = name.indexOf('.');
                    
                    if ('@' === name.substr(0, 1)) {
                        form_data.supporter.questions[name.substr(1)] = value;
                    }
                    else {
                        form_data.supporter[name] = value;
                    }
                    
                }
            }
            
            return form_data;

        }


                
        

		/*
		 *
		 */
        function displayError(msg) {
            if (typeof msg !== "object"
             || typeof msg.length === "undefined") {
                 msg = [msg];
            }

            settings.error(msg.join(settings.default_text_separator));
        }


              

        if (typeof options === 'object') {

            // on s'assure que des fonctions sont bien passées en option
            for (var f in ['success', 'error', 'init', 'complete', 'data']) {
                (options[f]
              && typeof options[f] !== 'function'
              && delete options[f]);
            }

            settings = $.extend(
                defaults,
                options
            );
        }
        else {
            settings = defaults;
        }


        var ids_array = ['pageId', 'clientId', 'campaignId'];

        for (var i in ids_array) {
            if (settings[ ids_array[i] ]) {
                settings[ ids_array[i] ] = parseInt(settings[ ids_array[i] ]);
            }
        }

        /*
         * Nos amis les placeholders
         */
        if (!placeholder) {
  		    selector.find('input[type=text]').each(function() {
  			    var $this = $(this),
  				    default_placeholder = $this.attr('placeholder');

  			    (function($this, default_placeholder) {
  				    $this
  				        .addClass('placeholder')
  				        .val( default_placeholder )
                        .on('focus', function() {
                            if ($this.val() === default_placeholder) {
  						        $this.val('')
  						          .removeClass('placeholder');
  					        }
  				        })
  				        .on('blur', function() {
  					        if ($this.val() === "") {
  						        $this.val(default_placeholder)
  						          .addClass('placeholder');
  					        }
  				        });

  			    })($this, default_placeholder);

  		    });
  	    }



        /*
         * Submit du formulaire
         */

        function onSubmit(e) {
            e.stopPropagation();
  		    e.preventDefault();

            var $this = $(this);
            var trim = $.trim;
            var serialized = $this.serializeArray();
			var form_var = {};
			var errors = [];
            
            // récupération des données du formulaires
            for (var i = 0, l = serialized.length; i < l; i++) {
                var j = serialized[i];
                form_var[ j.name ] = trim(j.value);
            }

            if (settings.pageId === 0) {
	            for (var en in ['en_error_url', 'en_success_url']) {
				    delete form_var[en];
			    }
            }
            

            // Vérifications.
            var missing = [];
            var invalid = [];

           
            // 1. champs obligatoires
            for (var m in settings.mandatory) {
                if (!form_var[m] || form_var[m] === '') {
                    missing.push(settings.mandatory[m]);
                }
            }

            if (missing.length > 0) {
                displayError(missing);
                return;
            }
            // fin champs obligatoires


            // 2. les validations
            for (var v in settings.validation) {
				if (settings.validation[v].regex
				 && form_var[v]
				 && ! form_var[v].match(settings.validation[v].regex)) {
					 invalid.push(settings.validation[v].message);
					 
				}

				if (settings.validation[v].validator
				 && form_var[v]
				 && typeof settings.validation[v].validator == "function"
				 && ! settings.validation[v].validator.call($this.find('[name="'+v+'"]')[0], form_var[v])) {
					 invalid.push(settings.validation[v].message);
					 
				}
            }

            if (invalid.length > 0) {
                displayError(invalid);
                return;
            }
            // fin validations

            
            if (settings.init() === false) {
                return;
            }


            form_var = settings.data(form_var);
			form_var = prepareSupporterData(form_var);
            
            if (settings.pageId === 0) {
                // V1 (legacy campaigns)
                
                $.get('https://e-activist.com/ea-action/action', form_var, function(data, status) {
                    if (status === "success"
				     && data.pages
				     && data.pages[0]
				     && data.pages[0].number
				     && data.pages[0].number > 1
				    ) {
                        settings.success();
                    }
                    else {
                        if (data.messages) {
                            var msg = [];

						    for (var i = 0, l = data.messages.length; i < l; i++) {
							    var message = data.messages[i];
								var error = message.error;
								var field = message.fieldName;
							    
							    msg.push(error);
								
						    }
						    
                            settings.error(msg);
                        }
                        else {
                            settings.error(settings.default_error);
                        }
                    }

                    settings.complete();

                }, 'jsonp');
            }
            else {
                // V2 (Pages)

                $.ajax({
                    method: 'get',
                    url: 'https://action.greenpeace.fr/ens/authenticate',
                    dataType: 'jsonp',
                    success: function(response) {
                        $.ajax({
                            method: 'post',
                            url: 'https://www.e-activist.com/ens/service/page/' + settings.pageId + '/process',
                            data: JSON.stringify(form_var),
                            dataType: 'json',
                            complete: function(data, status, c) {
                                var response = data.responseJSON;
                                if (status === 'error') {
                                    var msg;
                                    if (response.message) {
                                        msg = response.message;
                                    }
                                    else {
                                        msg = settings.default_error;
                                    }
                                    
                                    settings.error(msg);
                                }
                                else {
                                    if (response.status.toUpperCase() === "SUCCESS") {
                                        settings.success();
                                    }
                                    else {
                                        settings.error(settings.default_error);
                                    }
                                }

                                settings.complete();
                            },
                            headers: {
                                "Content-Type": "application/json",
                                "ens-auth-token": response.token
                            }
                        });    
                    }
                });
            }
        }
        
        selector.on('submit', onSubmit)
    };
})(jQuery);
