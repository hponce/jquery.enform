# INSTALLATION

1. npm install

2. grunt build

You need to have grunt installed globally.


# CONFIGURATION

Create your form where the input fields bears the name of the fields inside EN.

And add some hidden inputs with your client ID and the campaign ID.

Example :

	<form>
		<input type="hidden" name="ea.client.id" value="1234"/>
		<input type="hidden" name="ea.campaign.id" value="45678"/>
		
		<input type="text" name="email"/>
	...

And add some javascript :

	var options = { ... }; // see below

	$('form').enform(options);

You can also pass the client ID and campaign ID directly into the javascript options.

## OPTIONS

Nothing is required, but you should at least set the `error` and `success` callbacks.


### clientId
Type: `Number`

Your client ID in Engaging Networks. The same value as the parameter `ea.client.id`.


### campaignId
Type: `Number`

The ID of that campaign in Engaging Networks. The same value as the parameter `ea.campaign.id`.


### mandatory
Type: `Object`  
Default: `{}`

A list of fields and the corresponding error message when they are missing.
Example:

	{
		mandatory: {
			email: "Your email address is missing.",
			first_name: "We need you name."
		}
	}


### validation
Type: `Object`  
Default: `{}`

A list of fields containing a way to validate them and the corresponding error message. It can use regular expression (`regex` property) or a function (`validator` property).

Example:

	{
		validation: {
			email: {
				regex: /^[a-zA-Z0-9\._\-\+]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/, // simple regexp
				message: "Invalid email"
			}
		}
	}

or

	{
		validation: {
			email: {
				validator: function(value_of_field) {
					/* do something */
					return true; // or false if not valid
				}
				message: "Invalid email"
			}
		}
	}


### default_text_separator
Type: `String`  
Default: `<br/><br/>`

A string used to separate error messages. 


### init
Type: `function`

A function executed before submitting


### data
Type: `function`

A function to acces one argument, the object containing all the form fields and there value, executed before submitting the form.

Example:

	{
		data: function(vars) {
			// let's add a field to the submitted parameters
			vars.my_field = 123;
			
			return vars; // it must return the object that will be sent to EN.
		}
	}


### success
Type: `function`

A function executed when Engaging Networks returns correctly.


### error
Type: ̀function`

A function executed when an error is encountered. It retrieves the messages from th EN result.
One parameter is passed to the function, an array containing the messages from EN.
